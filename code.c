#include <stdio.h>
struct student {
    char FirstName[50];
    int roll;
    char Subject[50];
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter information of students:\n");

   
    for (i = 0; i < 5; ++i) {
        s[i].roll = i + 1;
        printf("\nFor roll number%d,\n", s[i].roll);
        printf("Enter First Name: ");
        scanf("%s", s[i].FirstName);
        printf("Enter Subject: ");
        scanf("%s", s[i].Subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("Displaying Information:\n\n");

    
    for (i = 0; i < 5; ++i) {
        printf("\nRoll number: %d\n", i + 1);
        printf("First Name: ");
        puts(s[i].FirstName);
        printf("Subject: %.50s", s[i].Subject);
        printf("\n");
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
        
    }
    return 0;
}
